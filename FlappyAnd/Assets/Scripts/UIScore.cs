﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIScore : PlayerUI
{
    private int prevScore;

    public override string ToString()
    {
        return prevScore.ToString();
    }

    protected override void UpdateScore()
    {
        t.text = ToString();
    }

    protected override void UpdateValue()
    {
        prevScore = b.score;
    }

    protected override bool ValueCheck()
    {
        return b.score != prevScore;
    }
}
