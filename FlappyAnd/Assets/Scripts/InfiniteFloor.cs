﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InfiniteFloor : MonoBehaviour
{
    public InfiniteFloor other;
    public GameObject target;
    public float xTarget = -6f;
    public bool hasMoved = false;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasMoved) {
            if (getRelativeX() > xTarget && getRelativeX() <= xTarget+1f) {
                other.transform.position += Vector3.right * this.transform.localScale.x;
                hasMoved = true;
                other.hasMoved = false;
            }
        }
    }
    private float getRelativeX() {
        //(-6)) - 0
        if (target)
            return target.transform.position.x - this.transform.position.x;
        else return 0;
    }
}
