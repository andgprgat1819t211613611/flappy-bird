﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    public PipeSet prefab;
    public List<PipeSet> pipeSetList = new List<PipeSet>();
    public Queue<PipeSet> pipeQ = new Queue<PipeSet>();
    public int maxNum = 10;
    public float rate = 2.0f;
    [SerializeField] private float time = 0.0f;
    public float variance = 0.0f, varChange = 1f / 40f;
    [SerializeField] private float prevSpawn = 0.5f;

    void Start()
    {
        for (int i = 0; i < maxNum; i++) {
            PipeSet p = Instantiate(prefab);
            p.psw = this;
            p.gameObject.SetActive(false);
            pipeQ.Enqueue(p);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!Game.instance.gameOver) {
            time += Time.deltaTime*rate;
            //print((pipeQ.Count > 0 && pipeSetList.Count < maxNum)+string.Format(": pipeQ:{0}, pipeL:{1}",pipeQ.Count, pipeSetList.Count));
            if (time >= 1.0f && (pipeQ.Count > 0 && pipeSetList.Count < maxNum)) {
                SpawnPipe();
                time = 0f;
            }
        }
    }

    public void SpawnPipe()
    {
        PipeSet p = pipeQ.Dequeue();
        prevSpawn = UnityEngine.Random.Range(prevSpawn - variance * prevSpawn, prevSpawn + variance * (1.0f - prevSpawn));
        p.SpawnSettings(prevSpawn, this.transform.position);
        pipeSetList.Add(p);
        p.gameObject.SetActive(true);
    }

    public void DestroyPipe(PipeSet p) {
        print("destroying pipe");
        p.gameObject.SetActive(false);
        p.gameObject.GetComponent<PipeScore>().scored = false;
        pipeSetList.Remove(p);
        pipeQ.Enqueue(p);
    }
    public void addVariance() {
        if (variance < 1.0f) {
            variance += varChange;
            if (variance > 1.0f)
                variance = 1.0f;
        }
    }

    private void OnDestroy()
    {
        while (pipeQ.Count > 0) {
            pipeSetList.Add(pipeQ.Dequeue());
        }
        while (pipeSetList.Count > 0) {
            PipeSet p = pipeSetList[0];
            pipeSetList.Remove(p);
            Destroy(p.pipeTop.gameObject);
            Destroy(p.pipeBottom.gameObject);
        }
    }
}
