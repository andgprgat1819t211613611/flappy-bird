﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public GameObject targ;
    public Vector3 offset;
    public Vector3 mult = new Vector3(1.0f, 0.0f, 1.0f);

	// Use this for initialization
	void Start () {
        offset = this.transform.position - targ.transform.position ;
	}
	
	// Update is called once per frame
	void Update () {
        if (!Game.instance.gameOver) {
            Vector3 temp = targ.transform.position;
            temp.Scale(mult);
            transform.position = temp + offset;
        }
	}
}
