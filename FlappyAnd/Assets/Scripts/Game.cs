﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{
    public static Game instance = null;
    public PipeSpawner spawn;
    public Bird player;
    public Canvas startScreen,gOverScreen;

    public bool gameOver = false;
    private bool gameStarted = false;


    private void Awake()
    {
        if (instance == null){
            instance = this;
        }
        else if (instance != this) {
            Destroy(this);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        if (!player)
            player = GameObject.Find("Player").GetComponent<Bird>();
        if (!spawn)
            spawn = GameObject.Find("pipeSpawner").GetComponent<PipeSpawner>();

        player.GetComponent<Rigidbody2D>().gravityScale = 0;
        spawn.enabled = false;
        PlatformDetect();
        player.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameOver)
        {
            if (!startScreen.gameObject.activeSelf)
            {
                if (!gameStarted)
                {
                    if (Input.anyKeyDown)
                    {
                        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                        {
                            StartGame();
                        }
                    }
                    if (Input.touchCount > 0)
                    {
                        if (Input.GetTouch(0).phase == TouchPhase.Began)
                        {
                            StartGame();
                        }
                    }
                }
                else
                {
                    player.speed = 5f + spawn.variance * 5f;
                    player.RefreshSpeed();
                }

            }
        }
        else {
            if (!gOverScreen.gameObject.activeSelf){
                gOverScreen.gameObject.SetActive(true);
            }
        }
        
        /*
        else{
            if (!gOverScreen.gameObject.activeSelf) {
                gOverScreen.gameObject.SetActive(true);
            }
            if (Input.anyKeyDown){
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)){
                    RestartGame();
                }
            }
            if (Input.touchCount > 0){
                if (Input.GetTouch(0).phase == TouchPhase.Began){
                    RestartGame();
                }
            }
        }*/
    }
    private void PlatformDetect() {
        //if (Application.platform == RuntimePlatform.WindowsEditor)
        switch (Application.platform) {
            case RuntimePlatform.Android:
            case RuntimePlatform.IPhonePlayer: player.jumpForce /=Mathf.Sqrt(2f);
                break;
        }
    }

    public void StartGame() {
        gameStarted = true;
        player.enabled = true;
        player.GetComponent<Rigidbody2D>().gravityScale = 2;
        player.Jump();
        spawn.enabled= true;
        startScreen.gameObject.SetActive(false);
    }
    public void StartGameHard(){
        StartGame();
        spawn.varChange = 1f / 20f;
        spawn.variance = 0.5f;
    }
    public void RestartGame() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    private void OnDestroy()
    {

    }
}
