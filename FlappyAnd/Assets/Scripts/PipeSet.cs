﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSet : MonoBehaviour
{
    public static float minY = -2f, maxY = 3f, narrowRange = 0.5f;
    public GameObject pipeTop, pipeBottom;
    public Queue<GameObject> q;
    public PipeSpawner psw;
    public Vector2 origin;

    // Start is called before the first frame update
    void Start()
    {
        if(!pipeTop || !pipeBottom)
        for (int i = 0; i < transform.childCount; i++) {
            if (transform.GetChild(i).transform.position.y > 0f)
                pipeTop = transform.GetChild(i).gameObject;
            else
                pipeBottom = transform.GetChild(i).gameObject;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(origin.x - psw.transform.position.x) > 30f) {
            psw.DestroyPipe(this);
        }
    }

    public float randomY(float seed) {
        return (maxY - minY) * seed + minY;
    }
    public float narrowRangeRand() {
        return Random.value * narrowRange;
    }

    public void SpawnSettings(float ySeed, Vector2 origin) {
        this.transform.position = new Vector3(origin.x, randomY(ySeed));
        pipeTop.transform.position = this.transform.position + Vector3.up*(7.0f + narrowRangeRand());
        pipeBottom.transform.position = this.transform.position + Vector3.down * (7.0f + narrowRangeRand());
        this.origin = origin;
    }
}
