﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour {
    private Rigidbody2D rb2d;
    public float jumpForce = 10;
    public float speed;
    public int score = 0;

    // Use this for initialization
    void Start() {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.right * speed;
        //RefreshSpeed();
    }

    private void FixedUpdate()
    {
        //print(string.Format("birdSpeed: {0}\nvel: {1}", speed, rb2d.velocity.x));
        if (!Game.instance.gameOver)
        {
            //moveRight();
        }
    }
    // Update is called once per frame
    void Update() {

        if (!Game.instance.gameOver) {
            if (Input.anyKeyDown) {
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0)) {
                    Jump();
                }
            }
            if (Input.touchCount > 0) {
                if (Input.GetTouch(0).phase == TouchPhase.Began) {
                    Jump();
                }
            }
        }
    }
    private void moveRight() {
        rb2d.position += -Vector2.left * speed * Time.fixedDeltaTime;
    }
    public void RefreshSpeed() {
        rb2d.velocity = new Vector2(Mathf.Abs(speed), rb2d.velocity.y);
    } 
    public void Jump() {
        //rb2d.AddForce(Vector2.up * jumpForce);
        if (rb2d.velocity.y <= 0f)
            rb2d.velocity = new Vector2(rb2d.velocity.x, jumpForce);
        else
            rb2d.velocity += Vector2.up * jumpForce;
    }
    public void PipeHit() {
        Game.instance.gameOver = true;
    }
    public void Score() {
        if(!Game.instance.gameOver)
            score++;
    }
}
