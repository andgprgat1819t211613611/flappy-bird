﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeScore : MonoBehaviour
{
    public bool scored = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D cl)
    {
        Bird b = cl.GetComponent<Bird>();
        if (b == !scored) {
            GetComponent<PipeSet>().psw.addVariance();
            b.Score();
            scored = true;
            //scoring goes here
        }

    }
}
